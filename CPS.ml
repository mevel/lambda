(* The source calculus. *)
module S = Lambda
(* The target calculus. *)
module T = Tail

(** [kont] is the type of a continuation value, as used by the CPS translator;
    it is either a variable or a λ-abstraction. The distinction is needed
    firstly because λ-astractions are not values in the target calculus, and
    secondly because of the definition of the “smart application”. **)

type kont =
  | KVar of T.variable
  | KLam of string * (T.value -> T.term)
      (* λ-abstractions are represented as OCaml functions, and are annotated
         with the name to give to its parameter, for generating code that is
         easier to read. *)

(** [kfun] is a handy function. **)

let kfun ?(param="x") k : kont =
  KLam (param, k)

(** [apply_kont w v] is the “smart application” of a continuation value [w] to
    a value [v]. This technique greatly improves the CPS translation by removing
    all administrative redexes.
    (This is noted w @ v in the course’s slides.) **)

let apply_kont (w : kont) (v : T.value) : T.term =
    begin match w with
    | KVar k ->
        T.TailCall (T.VVar k, [ v ])
    | KLam (_, k) ->
        k v
    end

(** Because λ-abstractions (“blocks”) are not values in the target calculus, we
    need to bind them to variables before using them in expressions. **)

let bind_block ?(name="blk") (b : T.block) : (T.value -> T.term) -> T.term =
  fun context ->
    let x =
      (* Make sure that fixpoints just under a let-binding use the same
       * identifier (this was already the case in the source language): *)
      begin match b with
      | T.Lam (T.Self f, _, _) -> f
      | _                      -> Atom.fresh name
      end in
    T.LetBlo (x, b, context (T.VVar x))

let bind_kont ?(name="kont") (w : kont) : (T.value -> T.term) -> T.term =
  fun context ->
    begin match w with
    | KVar k ->
        context (T.VVar k)
    | KLam (param, k) ->
        let x = Atom.fresh param in
        let lam = T.Lam (T.NoSelf, [ x ], k (T.VVar x)) in
        bind_block ~name lam context
    end

(** [cps_value v] is the CPS translation of value [v].
    (This is noted (| v |) in the course’s slides.) **)

let cps_value (v : S.term) : T.value =
  begin match v with
  | S.Var x ->
      T.VVar x
  | S.Lit n ->
      T.VLit n
  | _ ->
      assert false (* only those cases are given to [cps_value] by [cps_term] *)
  end

let rec cps_block (lam : S.term) : T.block =
  begin match lam with
  | S.Lam (self, x, t) ->
      (*    (| λx. t |)  =  λx,k. [| t |] {k}    *)
      let k = Atom.fresh "k" in
      T.Lam (self, [ x ; k ], cps_term t (KVar k))
  | _ ->
      assert false (* only this case is given to [cps_value] by [cps_term] *)
  end

(** [cps_term t w] is the CPS translation of term [t] with continuation value
    [w].
    (This is noted [| t |] {w} in the course’s slides.) **)

and cps_term (t : S.term) (w : kont) : T.term =
  begin match t with
  | S.Var _
  | S.Lit _ ->
      (*    [| v |] {w}  =  w @ (| v |)    *)
      apply_kont w (cps_value t)
  | S.Lam _ ->
      bind_block ~name:"lambda" (cps_block t) begin fun f ->
        apply_kont w f
      end
  | S.App (t1, t2) ->
      (*    [| t1 t2 |] {w}  =  [| t1 |] {λf. [| t2 |] {λx. f x w}}    *)
      cps_term t1 begin kfun~param:"called"@@fun f ->
        cps_term t2 begin kfun~param:"callarg"@@fun x ->
          bind_kont w begin fun k ->
            T.TailCall (f, [ x ; k ])
          end
        end
      end
  | S.BinOp (t1, op, t2) ->
      cps_term t1 begin kfun~param:"loperand"@@fun x ->
        cps_term t2 begin kfun~param:"roperand"@@fun y ->
          apply_kont w (T.VBinOp (x, op, y))
        end
      end
  | S.Print t1 ->
      cps_term t1 begin kfun~param:"printed"@@fun x ->
        T.Print (x,
          apply_kont w x
        )
      end
  (* We special-case the let-fun case; otherwise we would end up with
     something like:
         LetBlo ("lambda", translated_block,
         LetVal (f, "lambda",
         translated_t2 ))
     where "lambda" is a fresh variable, but this would introduce an useless and
     meaningless name, and would block some future optimizations (specifically,
     if the function is made global, it would be with the name "lambda", and
     calls to it with the name [f] wouldn’t be optimized). Instead, we want:
         LetBlo (f, translated_block,
         translated_t2 )
  *)
  | S.Let (f, (S.Lam _ as lam), t2) ->
      T.LetBlo (f, cps_block lam, cps_term t2 w)
  | S.Let (x, t1, t2) ->
      (*    [| let x = t1 in t2 |] {w}  =  [| t1 |] {λx’. let x = x’ in [| t2 |] {w}}    *)
      cps_term t1 begin kfun~param:(Atom.hint x)@@fun x' ->
        T.LetVal (x, x', cps_term t2 w)
      end
  | S.IfZero (t1, t2, t3) ->
      cps_term t1 begin kfun~param:"cond"@@fun b ->
        T.IfZero (b,
          cps_term t2 w,
          cps_term t3 w
        )
      end
  (** Extending the language with tuple… **)
  (* (1) Optimization: immediate deconstruction of tuples. *)
  | S.LetTup (xs, S.Tup ts, t) ->
      assert (List.length xs = List.length ts) ; (* thanks to the type system *)
      (* This enforces left-to-right evaluation (no variable is captured,
       * because they are all distinct): *)
      let t' = List.fold_right2 (fun xi ti t -> S.Let (xi, ti, t)) xs ts t in
      cps_term t' w
  (* (2) Optimization: 1-tuples are unboxed. *)
  (* This is sound because we treat all the cases of the source language that
     allow to construct or destruct tuples ([S.Tup] and [S.LetTup] are the only
     such cases). Hence all 1-tuple are built unboxed, and read as such. Because
     of the type system, a translated program will never try to read as a boxed
     n-tuple (n ≠ 1) a value that was an unboxed 1-tuple, nor conversely. *)
  | S.Tup [ t1 ] ->
      cps_term t1 w
  | S.LetTup ([ x1 ], t1, t2) ->
      cps_term (S.Let (x1, t1, t2)) w
  (* (2b) Optimization: 0-tuples are discarded. *)
  (* Same justification. This avoids allocating blocks to represent values of
     type [unit], and guarantees physical equality over all such values. *)
  | S.Tup [] ->
      apply_kont w (T.VLit 42)
  | S.LetTup ([], t1, t2) ->
      cps_term t1 begin kfun~param:"unit"@@fun _ ->
        cps_term t2 w
      end
  (* (3) The general cases. *)
  | S.Tup ts ->
      cps_term_list ts begin fun vs ->
        bind_block ~name:"tuple" (T.Tup vs) begin fun u ->
          apply_kont w u
        end
      end
  | S.LetTup (xs, t1, t2) ->
      cps_term t1 begin kfun~param:"boundtuple"@@fun v ->
        T.LetTup (xs, v, cps_term t2 w)
      end
  end

(* [cps_term_list [t1; …; tn] context] iterates the CPS translation on a list of
   [n] terms, evaluating them from left to right. [context] is to be thought as
   a “generalized” kontinuation: it receives the list of [n] values representing
   the results of the [n] terms, in order.
   This really is a generalisation of the process that has been used, for
   example, with [n = 2] to implement the case [BinOp], above. *)

and cps_term_list (ts : S.term list) (context : T.value list -> T.term) : T.term =
  begin match ts with
  | [] ->
      context []
  | t :: ts' ->
      cps_term t begin kfun~param:"listelt"@@fun v ->
        cps_term_list ts' begin fun vs ->
          context (v :: vs)
        end
      end
  end

let cps_term (t : S.term) : T.term =
  cps_term t begin kfun~param:"_"@@fun _ ->
    T.Exit
  end
