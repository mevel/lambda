(* This module typechecks the language [RawLambda].

   As a side effect, this checks that all variables are properly bound, thus
   discharging [Cook] from that task.

   With the tuple extension, it also checks that variables in a [LetTuple]
   pattern are all distinct.

   Since all intermediate languages were provided untyped, we just leave the
   language [RawLambda] untouched. This compiler pass simply fails with an error
   message if the term is not typeable.

   An improvement would be to add the types to the subsequent languages; this
   would allow to check invariants (including the validity of the type inference
   itself) and enable type-directed optimizations.
 *)

val typecheck_term : RawLambda.term -> RawLambda.term
