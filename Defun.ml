(* The source calculus. *)
module S = Tail
(* The target calculus. *)
module T = Top

(** [rename x y t] builds the term of the target language [let x = y in t].
    Remark: those renamings are optimized away in a subsequent compiler pass.**)

let rename (x : T.variable) (y : T.variable) (t : T.term) =
  T.LetVal (x, T.VVar y, t)

(** [multi_rename [x1;…;xn] [y1;…;yn] t] builds the following term:
    [   let x1 = y1 in
        let x2 = y2 in
        …
        let xn = yn in
        t              ]
**)

let multi_rename : T.variable list -> T.variable list -> T.term -> T.term =
  List.fold_right2 rename

(** There is no need to add any construct to the target calculus [Top] in order
    to implement tuples, because it already has switches and memory blocks,
    which are precisely what we need. However we need to provide a dummy tag for
    blocks that hold tuples. We use [tuple_tag].

    A possible optimization of the generated C code would be not to test the tag
    at all, and read the tuple directly. To implement this we need to add
    support for default branches in the language [Top].
 **)

let tuple_tag = 0

(** A record [apply] represents the global function used for defunctionalizing
    all closures of a given arity [n], where [n = apply.arity]. The name of the
    global function is [apply.name]. Its arity is [1+n].

    1. Its first parameter has name [apply.block_param] and is the closure to
       apply (of type [T.block]).

    2. Its [n] remaining parameters have names [apply.params] and are the
       parameters of the closure.

    Thus we have the following invariant (enforced by [make_apply]):

        [List.length apply.params = apply.arity]

    The code of the “apply” function is a switch. Each branch corresponds to a
    closure of arity [n] found in the source code. [apply.last_tag] is the
    number of such closures that has been met already, hence we also maintain
    the following invariant (enforced by [add_branch]):

        [List.length apply.branches = apply.last_tag]
 **)

type apply =
  {
    arity : int ;
    name  : T.variable ;
    block_param : T.variable ;
    params      : T.variable list ;
    mutable last_tag : int ;
    mutable branches : T.branch list ;
  }

let make_apply n =
  {
    arity = n ;
    name  = Atom.fresh @@ Util.indexed_name "apply" n ;
    block_param = Atom.fresh "closure" ;
    params      = Util.list_init n (fun i -> Atom.fresh @@ Util.indexed_name "param" i) ;
    last_tag = 0 ;
    branches = [] ;
  }

let add_branch (apply : apply) (fv : T.variable list) (t : T.term) : T.tag =
  let tag = apply.last_tag in
  apply.last_tag <- succ tag ;
  apply.branches <- T.Branch (tag, fv, t) :: apply.branches ;
  tag

let compile_apply (apply : apply) : T.function_declaration =
  T.Fun (apply.name, apply.block_param :: apply.params,
    T.Swi (T.VVar apply.block_param, List.rev apply.branches)
  )

(** [defun_term] is the main defunctionalization function. Because the
    translation is stateful, we need to define most functions locally. **)

let defun_term (t : S.term) : T.program =

  (** The state of the defunctionalization is the set of “apply” handlers that
      have been created already. We index them by their arity. **)

  let apps : (int, apply) Hashtbl.t =
    Hashtbl.create 8
  in

  let get_apply n =
    begin match Hashtbl.find apps n with
    | apply               -> apply
    | exception Not_found ->
        let apply = make_apply n in
        Hashtbl.add apps n apply ;
        apply
    end
  in

  (** The state also includes a set of global declarations. Hence we can
      optimize things by compiling some closures into global functions.
      (“apply” handlers are not included because they will be added at the end
      of the algorithm, since they are defined on-the-fly.) **)

  let globals : T.function_declaration list ref =
    ref []
  in

  let add_global global_name xs t =
    globals := (T.Fun (global_name, xs, t)) :: !globals
  in

  (** The table [globalized] keeps track of which closures have been hoisted to
      global functions. Because we might still need to use them as first-class
      closures in some places, we also need to have them represented as blocks.
      So if [name] refers to a closure in the source program that has been
      compiled into a global function, then the table [globalized] maps [name]
      to a pair [(global_name, block)] where [global_name] is the name of the
      global function, and [block] is a block that represents it.

      This is unfortunate: a much better way would be to have the [block] bound
      once and for all to a global C variable, just like the global function.
      That way, the block would be available from everywhere with no dynamic
      allocation. Sadly, the C backend does not support global variables, so
      instead we must bind [name] to [block] everywhere it might be needed, at
      the cost of painfulness and dynamic allocations.

      [bind_globals] is an helper function for that: if all names [x_i]s are in
      [globalized], then [bind_globals [ x1 ; … ; xn ] t] is the term [t] where
      the names [x_i] have been bound to their respective blocks.

      Note that we optimize away superfluous block bindings (hence, allocations)
      in a subsequent optimization pass.
      **)

  let globalized : (T.variable, T.variable * T.block) Hashtbl.t =
    Hashtbl.create 32
  in

  let bind_global_blocks =
    List.fold_right begin fun name t ->
      let (_, block) = Hashtbl.find globalized name in
      T.LetBlo (name, block, t)
    end
  in

  (** The actual translation functions. **)

  let rec translate_value (v : S.value) : T.value =
    v

  (** All the hard work is done when translating closures… **)

  and translate_closure name (self, params, body) : T.block =
    (* An invariant of the compilation: *)
    begin match self with
    | S.NoSelf -> ()
    | S.Self f -> assert (Atom.equal name f)
    end ;
    let apply = get_apply (List.length params) in
    (* [gv] is the list of global variables that appear in the [body] term;
       [fv] is the list of free variables, which does not include the global
       variables because they are bound at toplevel and mutually recursive. *)
    let (gv, fv) =
      S.Lam (self, params, body)
      |> S.fv_block
      |> Atom.Set.elements
      |> List.partition (Hashtbl.mem globalized)
    in
    (* If there are no free variables, we hoist the closure to a global
       function: *)
    if fv = [] then begin
      let global_name = Atom.fresh ("global_" ^ Atom.hint name) in
      let branch_body = T.TailCall (global_name, T.vvars apply.params) in
      let tag = add_branch apply [] branch_body in
      let block = T.Con (tag, []) in
      Hashtbl.add globalized name (global_name, block) ;
      (* We need to bind [name] in [globalized] *before* translating [body],
         in case [body] uses [name] recursively. *)
      let () =
        body
        |> translate_term
        |> bind_global_blocks (name :: gv)
        |> add_global global_name params
      in
      block
    (* Otherwise, we compile it normally: *)
    end else begin
      let tag =
        body
        |> translate_term
        |> bind_global_blocks gv
        |> multi_rename params apply.params
        |>  begin match self with
            | S.NoSelf -> fun x -> x
            | S.Self f -> rename f apply.block_param
            end
        |> add_branch apply fv
      in
      T.Con (tag, T.vvars fv)
    end

  and translate_block name (block : S.block) : T.block =
    begin match block with
    | S.Lam (self, params, body) ->
        translate_closure name (self, params, body)
    (* Extending the language with tuples: *)
    | S.Tup vs ->
        T.Con (tuple_tag, vs)
    end

  and translate_term (t : S.term) : T.term =
    begin match t with
    | S.Exit ->
        T.Exit
    | S.TailCall (v, vs) ->
        (* This is where we take advantage of the global functions: they can be
           called directly, without resorting to blocks and “apply” handlers. *)
        let name =
          begin match v with
          | T.VVar x -> x
          | _        -> assert false (* forbidden by the type system *)
          end in
        begin match Hashtbl.find_opt globalized name with
        | Some (global_name, _) ->
            T.TailCall (global_name, List.map translate_value vs)
        | None ->
            let apply = get_apply (List.length vs) in
            T.TailCall (apply.name, translate_value v :: List.map translate_value vs)
        end
    | S.Print (v, t) ->
        T.Print (translate_value v, translate_term t)
    (* This optimization is normally done at other steps of the compilation, but
       better safe than sorry; this is useful for detecting more calls to
       globalized functions: *)
    | S.LetVal (x, S.VVar y, t) ->
        translate_term (S.subst_in_term x (S.VVar y) t)
    | S.LetVal (x, v, t) ->
        T.LetVal (x, translate_value v, translate_term t)
    | S.LetBlo (x, blo, t) ->
        let blo' = translate_block x blo in
        T.LetBlo (x, blo', translate_term t)
    | S.IfZero (v1, t2, t3) ->
        T.IfZero (translate_value v1, translate_term t2, translate_term t3)
    (* Extending the language with tuples: *)
    | S.LetTup (xs, v, t) ->
        let branch = T.Branch (tuple_tag, xs, translate_term t) in
        T.Swi (translate_value v, [ branch ])
    end
  in

  (** Now perform the translation for real! **)

  let t' = translate_term t in
  apps |> Hashtbl.iter begin fun _ apply ->
    globals := compile_apply apply :: !globals
  end ;
  T.Prog (!globals, t')
