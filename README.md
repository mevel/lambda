## Additional features

More detailed comments can be found in the source files.


### IfZero

Added test files:

 +  `if_nested.lambda`
 +  `if_fib.lambda`


### CPS

`CPS.ml` implements the “smart application” variant of CPS translation. This is
the most efficient translation possible as it produces no extra redex.

In particular, expressions of the source program that syntactically map to
“values” of the target language, are translated as themselves. For example,
`3 + (4 * x)` gets translated to `3 + (4 * x)` instead of, say,

    let a = 3 in
    let b
      let c = 4 in
      let d = x in
      c * d
    in
    a + b

A first version of this used substitutions in the target language `Tail`. It is
is no longer the case, but the implementation of substitutions has been kept
(`Tail.ml`, lines 169 to end of file) because it is used in `Defun.ml` as an
optimization.


### Elimination of some bindings

A simplification of `Top` programs has been added as a compilation pass
(`Main.ml`, line 92).

It removes some superfluous bindings such as variable-to-variable bindings, but
also variable-to-value bindings when the value expression is small enough, or
when the variable is used no more than once in the subsequent term.

More importantly perhaps, it also removes variable-to-block bindings when they
are unused. This saves allocations.

This is implemented in `Top.ml` (lines 100 to end of file).

Added test file:

 +  `ren.lambda`


### Lightweight defunctionalization

The defunctionalization has been made more selective by taking advantage of the
mechanism of (mutually recursive) global functions, supported by the target
language.

A closure that has no free variables is now compiled to a global function, that
can be called directly instead of using a block (reified closure) and an “apply”
handler. For example:

    let id x = x in
    print (id 42)

Here `id` will be made global and, during defunctionalization, the call `id 42`
will be translated as a call to the global function `id`, instead of a call to
`apply` with a block representing `id`.

Such a closure won’t be taken into account when computing free variables of
subsequent terms (because global functions are bound everywhere), which will
in turn allow for more closures to be made global.

We still need to be able to manipulate those functions as ordinary closures, for
use as first-class functions. For example:

    let call f x = f x in
    let id x = x in
    print (call id 42)

Here, `id` will be made global (`call` too), but `call` can’t know whether its
parameter `f` is global or not.

Unfortunately, the target language supports global functions but not global
variables (although C does), which we would need to conveniently store the
blocks that represent our global functions. Instead, for a function called
"name", we must allocate a block and bind it to "name" everytime it is used
(that is, in the subsequent term, and also in the body of other global
functions, because these are taken out of their original environment and placed
toplevel, hence they do not have "name" anymore in their environment).

This is inefficient, although we rely on the optimization described in the
previous section to remove some unneeded allocations.

Lightweight defunctionalization is implemented in `Defun.ml` (lines 110-155,
plus `Defun.translate_closure` and case `TailCall` of `Defun.translate_term`).

Added test files:

 +  `global.lambda`
 +  `global_firstclass.lambda`


### Tuples (product types)

The language has been extended with tuples. Three syntactical constructs have
been added to the grammar (`Parser.mly`):

 +  `[term_1, …, term_n]` builds a tuple;
 +  `let [x1, …, xn] = term1 in term2` destructs a tuple and binds variables to
    its coordinates;
 +  `t1 ; t2` is syntactical sugar for `let [] = t1 in t2` (this adds a layer
    to the grammar, just above `any_term`).

Only flat patterns are supported. The compiler checks that a pattern does not
mention the same variable twice.

This required adding two constructs in languages `RawLambda`, `Lambda`, and
`Tail`, and extend all compiler passes from lexing down to defunctionalization.
Nothing has been added to `Top` nor after, because there we can take advantage
of the existing constructs for blocks and switches.

In `Tail`, tuples are blocks and not values, because they are allocated.

Some optimizations are done in `CPS.ml` (lines 146-171) to save allocations.

 +  Tuple-to-tuple bindings (in other words, `let [x1,…,xn] = [t1,…,tn] in …`),
    are simplified.
 +  0-tuples (that is, values of type `unit`) are replaced by a dummy value that
    is not allocated.
 +  1-tuples (which are allowed, although not terribly useful) are unboxed.
    Note however that the type system still consider `42` and `[42]` to be of
    different types.

Added test files:

 +  `tuple_simple1.lambda`
 +  `tuple_simple2.lambda`
 +  `tuple_fib.lambda`
 +  `tuple_ren.lambda` (to observe the optimization of tuple-to-tuple bindings)
 +  `tuple_sequencing.lambda` (to observe the sequencing syntax and the
    optimization of 0-tuples)
 +  `tuple_1tuple-1.lambda` (to observe the optimization of 1-tuples)
 +  `tuple_1tuple-2.lambda` (same)
 +  `tuple_1tuple_fail1.lambda` (supposed to fail at typechecking)
 +  `tuple_1tuple_fail2.lambda` (idem)


### Type system

A ML-style type system has been implemented. Now an additional compiler pass
(`Main.ml`, line 86) tries to infer types and rejects the program with a
detailed error message if it does not typecheck. The AST is left unchanged:
subsequent passes retain no typing information, although their safety often
relies on the source program being well-typed.

The implementation is in `Type.{ml,mli}`. This is algorithm W with unification.
In other words, let-polymorphism. Because of all the substitutions, it is
probably not very efficient on larger programs (a possible optimization would be
to periodically trim the environment down to what is needed to typecheck the
current term).

For recursive functions, there is only monomorphic recursion (since polymorphic
recursion makes type inference undecidable).

Value restriction has been implemented, although this is not necessary for the
source language, because it has no mutable memory (yet). This is an easy change
anyway.

Added test files:

 +  `type_fail1.lambda` (does not typecheck)
 +  `type_fail2.lambda` (same)
 +  `type_fail-if-valrestr.lambda` (typechecks only without the value restriction)
 +  `type_ok1.lambda` (always typecheck)
 +  `type_ok2.lambda` (always typecheck)

Note that `bool.lambda` fails to typecheck. This is expected (the error is the
same as the one given by OCaml).


## Notes

General-purpose functions of use in this project have been thrown in an
additional module called `Util`.
