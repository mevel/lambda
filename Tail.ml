(* This intermediate language describes the result of the CPS transformation.
   It is a lambda-calculus where the ordering of computations is explicit and
   where every function call is a tail call.

   The following syntactic categories are distinguished:

   1. "Values" include variables, integer literals, and applications of the
      primitive integer operations to values. Instead of "values", they could
      also be referred to as "pure expressions". They are expressions whose
      evaluation terminates and has no side effect, not even memory
      allocation.

   2. "Blocks" include lambda-abstractions. Even though lambda-abstractions
      are traditionally considered values, here, they are viewed as
      expressions whose evaluation has a side effect, namely, the allocation
      of a memory block.

      When we extend the language with tuples, these are also blocks, because
      they allocate.

   3. "Terms" are expressions with side effects. Terms always appear in tail
      position: an examination of the syntax of terms shows that a term can be
      viewed as a sequence of [LetVal], [LetTup], [LetBlo], [Print] and [IfZero]
      instructions, terminated with either [Exit] or [TailCall]. This implies,
      in particular, that every call is a tail call.

   In contrast with the surface language, where every lambda-abstraction has
   arity 1, in this calculus, lambda-abstractions of arbitrary arity are
   supported. A lambda-abstraction [Lam] carries a list of formal arguments
   and a function call [TailCall] carries a list of actual arguments. Partial
   applications or over-applications are not supported: it is the programmer's
   responsibility to ensure that every function call provides exactly as many
   arguments as expected by the called function. *)

type variable =
  Atom.atom

and self = Lambda.self =
  | Self of variable
  | NoSelf

and binop = Lambda.binop =
  | OpAdd
  | OpSub
  | OpMul
  | OpDiv

and value =
  | VVar of variable
  | VLit of int
  | VBinOp of value * binop * value

and block =
  | Lam of self * variable list * term
  (* Extending the language with tuples: *)
  | Tup of value list

(* Terms include the following constructs:

   - The primitive operation [Exit] stops the program.

   - The tail call [TailCall (v, vs)] transfers control to the function [v]
     with actual arguments [vs]. (The value [v] should be a function and its
     arity should be the length of [vs].)

   - The term [Print (v, t)] prints the value [v], then executes the term [t].
     (The value [v] should be a primitive integer value.)

   - The term [LetVal (x, v, t)] binds the variable [x] to the value [v], then
     executes the term [t].

   - The term [LetBlo (x, b, t)] allocates the memory block [b] and binds the
     variable [x] to its address, then executes the term [t].

   - The term [IfZero (v1, t2, t3)] tests the value [v1], executes the term [t2]
     if it is zero and [t3] otherwise. (The value [v1] should be a primitive
     integer value.)

   - The term [LetTup (xs, v, t)] binds the variables [xs] to the coordinates of
     the value [v], then executes the term [t].
 *)

and term =
  | Exit
  | TailCall of value * value list
  | Print of value * term
  | LetVal of variable * value * term
  | LetBlo of variable * block * term
  | IfZero of value * term * term
  (* Extending the language with tuples: *)
  | LetTup of variable list * value * term

[@@deriving show { with_path = false }]

(* -------------------------------------------------------------------------- *)

(* Constructor functions. *)

let vvar x =
  VVar x

let vvars xs =
  List.map vvar xs

(* -------------------------------------------------------------------------- *)

(* Computing the free variables of a value, block, or term. *)

open Atom.Set

let rec fv_value (v : value) =
  match v with
  | VVar x ->
      singleton x
  | VLit _ ->
      empty
  | VBinOp (v1, _, v2) ->
      union (fv_value v1) (fv_value v2)

and fv_values (vs : value list) =
  union_many fv_value vs

and fv_lambda (xs : variable list) (t : term) =
  diff (fv_term t) (of_list xs)

and fv_block (b : block) =
  match b with
  | Lam (NoSelf, xs, t) ->
      fv_lambda xs t
  | Lam (Self f, xs, t) ->
      remove f (fv_lambda xs t)
  | Tup vs ->
      fv_values vs

and fv_term (t : term) =
  match t with
  | Exit ->
      empty
  | TailCall (v, vs) ->
      fv_values (v :: vs)
  | Print (v1, t2) ->
      union (fv_value v1) (fv_term t2)
  | LetVal (x, v1, t2) ->
      union
        (fv_value v1)
        (remove x (fv_term t2))
  | LetBlo (x, b1, t2) ->
      union
        (fv_block b1)
        (remove x (fv_term t2))
  | IfZero (v1, t2, t3) ->
      union
        (fv_value v1)
        (union
          (fv_term t2)
          (fv_term t3)
        )
  | LetTup (xs, v1, t2) ->
      union
        (fv_value v1)
        (diff (fv_term t2) (of_list xs))

(* -------------------------------------------------------------------------- *)

(* Substitution of a value [s] for a variable [x]. This is completely
   straightforward (no α-renaming is needed) since all identifiers are
   guaranteed to be unique. *)

let rec subst_in_value (x : variable) (s : value) : value -> value = function
  | VVar y when Atom.equal x y ->
      s
  | VVar _ as v ->
      v
  | VLit _ as v ->
      v
  | VBinOp (v1, op, v2) ->
      VBinOp (subst_in_value x s v1, op, subst_in_value x s v2)

and subst_in_block (x : variable) (s : value) : block -> block = function
  | Lam (f, xs, t) ->
      Lam (f, xs, subst_in_term x s t)
  | Tup vs ->
      Tup (List.map (subst_in_value x s) vs)

and subst_in_term (x : variable) (s : value) : term -> term = function
  | Exit ->
      Exit
  | TailCall (v, vs) ->
      TailCall (subst_in_value x s v, List.map (subst_in_value x s) vs)
  | Print (v, t) ->
      Print (subst_in_value x s v, subst_in_term x s t)
  | LetVal (y, v, t) ->
      assert (not @@ Atom.equal x y) ;
      LetVal (y, subst_in_value x s v, subst_in_term x s t)
  | LetBlo (y, b, t) ->
      assert (not @@ Atom.equal x y) ;
      LetBlo (y, subst_in_block x s b, subst_in_term x s t)
  | IfZero (v1, t2, t3) ->
      IfZero (subst_in_value x s v1, subst_in_term x s t2, subst_in_term x s t3)
  | LetTup (ys, v, t) ->
      assert (not @@ List.exists (Atom.equal x) ys) ;
      LetTup (ys, subst_in_value x s v, subst_in_term x s t)
