(** General-purpose functions go here. **)

val indexed_name : string -> int -> string

val list_init : ?start:int -> int -> (int -> 'a) -> 'a list

(** Given a comparison function [cmp], [find_duplicate cmp li] returns a value
    that is found several times in list [li], if any. **)

val find_duplicate : ('a -> 'a -> int) -> 'a list -> 'a option
