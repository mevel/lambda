let indexed_name s n =
  s ^ string_of_int n ^ "_"

let rec list_init ?(start=0) stop f =
  if start >= stop then
    []
  else
    f start :: list_init ~start:(succ start) stop f

let find_duplicate (type a) cmp =
  let exception Return of a in
  let f x y =
    begin match cmp x y with
    | 0 -> raise (Return x)
    | c -> c
    end
  in
fun li ->
  begin match List.sort f li with
  | _                  -> None
  | exception Return x -> Some x
  end

