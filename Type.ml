(** Definition of types.

    We reuse facilities from [Atom] for type variables.

    A type [typ] is either a type variable [typvar] or the application of a type
    constructor [typconstr] to type parameters. Atomic types like [Int] are
    special cases of this, as type constructors of arity 0.

    A polytype [polytyp] is a type [ty] with a set of generalized (universally
    quantified) type variables [tvs].

    A typing environment [typenv] is a mapping from term variables to polytypes.
    Note that here, since the term language is RawLambda, term variables are
    raw strings.
 **)

type typvar = Atom.t

module TVSet : (module type of Atom.Set with type elt = typvar) = Atom.Set

type typconstr =
  | Int
  | Arrow
  (* Extending the language with tuples: *)
  | Tuple

[@@deriving show { with_path = false }]

type typ =
  | TVar of typvar
  | TApp of typconstr * typ list

type polytyp =
  {
    tvs : TVSet.t ;
    ty  : typ ;
  }

module Env = Map.Make (String)

type typenv = polytyp Env.t

(** Those functions help creating types. **)

let typ_int =
  TApp (Int, [])

let typ_arrow ty1 ty2 =
  TApp (Arrow, [ ty1 ; ty2 ])

let typ_tuple tys =
  TApp (Tuple, tys)

let typ_unit =
  typ_tuple []

let fresh_typvar () =
  TVar (Atom.fresh "T")

(** Those functions compute the set of free type variables of a type. **)

let rec ftv_typ ty =
  begin match ty with
  | TVar tv           ->  TVSet.singleton tv
  | TApp (_, tparams) ->  TVSet.union_many ftv_typ tparams
  end

let ftv_polytyp pty =
  TVSet.diff (ftv_typ pty.ty) pty.tvs

let ftv_typenv env =
  TVSet.union_many (fun (_, pty) -> ftv_polytyp pty) (Env.bindings env)

let typvar_not_free_in_typ tv ty =
  not (TVSet.mem tv (ftv_typ ty))

(** Pretty-printing.

    This is useful only for error messages.

    Using unique names generated from [Atom], with numeric identifiers which are
    globally unique, leads to illegible outputs. So given the set of useful
    variables (the one that appear in the output) we generate alphanumeric names
    for them. Free type variables get uppercase names, of the form:

        ?A, ?B, …, ?Z, ?A1, ?B1, …, ?Z1, ?A2, …

    Generalized type variables get lowercase names, of the form:

        'a, 'b, …, 'z, 'a1, 'b1, …, 'z1, 'a2, …
 **)

let gen_name_for_nth_typvar n =
  Printf.sprintf "?%c%s"
    (Char.chr (Char.code 'A' + (n mod 26)))
    (if n < 26 then "" else string_of_int (n / 26))

let gen_name_for_nth_polytypvar n =
  Printf.sprintf "'%c%s"
    (Char.chr (Char.code 'a' + (n mod 26)))
    (if n < 26 then "" else string_of_int (n / 26))

let gen_names_for_typvars tvs =
  tvs
  |> TVSet.elements
  |> List.mapi (fun i tv -> (tv, gen_name_for_nth_typvar i))

let gen_names_for_polytypvars tvs =
  tvs
  |> TVSet.elements
  |> List.mapi (fun i tv -> (tv, gen_name_for_nth_polytypvar i))

let show_typvar ~names tv =
  begin try
    List.assoc tv names
  with Not_found ->
    (* should not happen *)
    Printf.sprintf "'%s_%d" (Atom.hint tv) (Atom.identity tv)
  end

(* [show_typconstr] is only used as a failsafe for the pretty-printer, all type
   constructors should be treated specially. *)

let show_typconstr tconstr =
  Printf.sprintf "#%s" (show_typconstr tconstr)

(* [show_lvl lvl ~names ty] allows for correct parenthesizing of the expression
   representing type [ty], based on the level [lvl] in which it is expected to
   be nested; for this to work, [show_typ ~names ty] returns a pair [(lvl',str)]
   where [str] is a string representing [ty] and [lvl'] is the maximal outer
   level for which [str] needs outer parentheses. *)

let rec show_lvl lvl ~names ty =
  let (lvl', str) = show_typ ~names ty in
  if lvl' < lvl then
    str
  else
    Printf.sprintf "(%s)" str

and show_typ ~names ty =
  begin match ty with
  | TVar tv ->
      0,
      show_typvar ~names tv
  | TApp (Int, []) ->
      0,
      "int"
  | TApp (Arrow, [ ty1 ; ty2 ]) ->
      90,
      Printf.sprintf "%s → %s" (show_lvl 90 ~names ty1) (show_lvl 91 ~names ty2)
  | TApp (Tuple, []) ->
      0,
      "unit"
  | TApp (Tuple, [ ty ]) ->
      10,
      Printf.sprintf "tuple1 %s" (show_lvl 10 ~names ty)
  | TApp (Tuple, tys) ->
      50,
      String.concat " × " (List.map (show_lvl 50 ~names) tys)
  | TApp (tconstr, []) ->
      0,
      show_typconstr tconstr
  | TApp (tconstr, tys) ->
      10,
      String.concat " " (show_typconstr tconstr :: List.map (show_lvl 10 ~names) tys)
  end

let show_typ =
  show_lvl 100

let show_polytyp ~names pty =
  let ptv_names = gen_names_for_polytypvars pty.tvs in
  begin match ptv_names with
  | [] ->  Printf.sprintf "%s"
  | _  ->  Printf.sprintf "∀ %s.  %s" (String.concat ", " (List.map snd ptv_names))
  end
    (show_typ ~names:(List.append ptv_names names) pty.ty)

let show_typenv ~names env =
  env
  |> Env.bindings
  |> List.map begin fun (x, pty) ->
       Printf.sprintf "%s : \t%s" x (show_polytyp ~names pty)
     end
  |> String.concat "\n    "

(** Substitutions of type variables within types.

    Substitutions are (finite) mappings from type variables to types, such that
    their extension to types is involutive. Said otherwise, no type variable of
    the domain appears in one of the images.

    To preserve this property when composing, [Subst.compose σ2 σ1] is
    restricted to the case where the second substitution [σ2] is invariant by
    the first one [σ1] (that is, no variable of the domain of [σ1] occurs in an
    image of [σ2]).

    Also note that we will only compose substitutions with disjoint domains.
    This is not required by the property above, but is a meaningful invariant of
    the unification algorithm below: otherwise, if [x] was in both domains, then
    we would lose the equation [x = σ2(x)] when composing [σ1] with [σ2],
    because we prefer [σ1] over [σ2] for handling conflicts.

    [renaming tvs] is a substitution which remaps all variables of the set [tvs]
    to fresh variables.
 **)

module Subst : sig
  type t
  val id : t
  val singleton : typvar -> typ -> t
  val renaming : TVSet.t -> t
  val compose : t -> t -> t
  val apply_in_typ     : t -> typ     -> typ
  val apply_in_polytyp : t -> polytyp -> polytyp
  val apply_in_typenv  : t -> typenv  -> typenv
end = struct

  module M : (module type of Atom.Map with type key = typvar) = Atom.Map

  type t = typ M.t

  let id =
    M.empty

  let singleton tv ty =
    assert (typvar_not_free_in_typ tv ty) ;
    M.singleton tv ty

  let renaming tvs =
    TVSet.fold (fun tv -> M.add tv (fresh_typvar ())) tvs M.empty

  let rec apply_in_typ s ty =
    begin match ty with
    | TVar a ->
        begin match M.find_opt a s with
        | None     ->  ty
        | Some ty' ->  apply_in_typ s ty'
        end
    | TApp (tconstr, tparams) ->
        TApp (tconstr, List.map (apply_in_typ s) tparams)
    end

  let apply_in_polytyp s pty =
    (* The substitution under quantifiers is straightforward here because we
       use fresh variables everywhere, no capture takes place. *)
    assert (s |> M.for_all begin fun tv ty ->
      not (TVSet.mem tv pty.tvs)
      && TVSet.disjoint (ftv_typ ty) pty.tvs
    end) ;
    { pty with ty = apply_in_typ s pty.ty }

  let apply_in_typenv s env =
    Env.map (apply_in_polytyp s) env

  let compose s2 s1 =
    assert (s1 |> M.for_all begin fun tv1 _ ->
      s2 |> M.for_all begin fun _ ty2 ->
        typvar_not_free_in_typ tv1 ty2
      end
    end) ;
    (* As domains are disjoint in practical use, [handle_conflicts] is in fact
       never used. *)
    let handle_conflicts _tv _ty2 ty1 = Some ty1 in
    M.union handle_conflicts s2 (M.map (apply_in_typ s2) s1)

end

let ( @  ) = Subst.compose
let ( @. ) = Subst.apply_in_typ
let ( @: ) = Subst.apply_in_typenv

(** Unification.

    If [ty1] and [ty2] admit a most general unifier, then [mgu ty1 ty2] returns
    it as a substitution; otherwise it raises an exception
        [Not_unifiable (σ, ty1', ty2')]
    where:
        [σ] is the substitution computed so far,
        [ty1'] is a type contained syntactically in [σ(ty1)],
        [ty2'] is a type contained syntactically in [σ(ty2)],
    such that [ty1'] and [ty2'] can’t be unified.

    Implementing [mgu] requires a more general function that takes as input a
    list of equations [eqs]. For it to be tail-recursive, it also takes the
    substitution computed so far in an accumulator.
 **)

exception Not_unifiable of Subst.t * typ * typ

let rec mgu s (eqs : (typ * typ) list) =
  begin match eqs with
  (* no equation left *)
  | [] ->
      s
  (* equation [x =?= x] *)
  | (TVar x, TVar y) :: eqs' when Atom.equal x y ->
      mgu s eqs'
  (* equation [x =?= t] or [t =?= x] where term [t] does not contain variable [x] *)
  (* NOTE: OCaml issues a warning here because of the or-pattern under a guard;
     This is fine here because, if the equation matches the first pattern and
     fails the side condition, it can’t be of the form [x =?= y] where [x] and
     [y] are different variables, which is the only case in which it would also
     match the second pattern.
     This trick serves to factor the code without calling [mgu] with a swapped
     equation, which would mess up left and right sides in error reporting. *)
  | (TVar x, ty) :: eqs'
  | (ty, TVar x) :: eqs'
    when typvar_not_free_in_typ x ty ->
      let u = Subst.singleton x ty in
      let subst_in_eq (ty1, ty2) = (u @. ty1, u @. ty2) in
      mgu (u @ s) (List.map subst_in_eq eqs')
  (* equation [F(t1,…,tn) =?= F(u1,…,un)] *)
  | (TApp (tconstr1, tparams1), TApp (tconstr2, tparams2)) :: eqs'
    when tconstr1 = tconstr2 && List.length tparams1 = List.length tparams2 ->
      mgu s (List.append (List.combine tparams1 tparams2) eqs')
  (* non-unifiable equation *)
  | (ty1, ty2) :: _ ->
      raise (Not_unifiable (s, ty1, ty2))
  end

let mgu ty1 ty2 =
  mgu Subst.id [ (ty1, ty2) ]

(** Generalize / instantiate.

    [polytyp_of_typ ty] creates the polytype corresponding to type [ty], with no
    variable generalized.

    [generalize env ty] generalizes all type variables in type [ty] that do not
    occur in typing environment [env].

    [instantiate pty] instantiates the polytype [pty] into a type by replacing
    all quantified variables with fresh variables.
 **)

let polytyp_of_typ ty =
  { tvs = TVSet.empty ; ty = ty }

let generalize env ty : polytyp =
  {
    tvs = TVSet.diff (ftv_typ ty) (ftv_typenv env) ;
    ty  = ty
  }

let instantiate pty : typ =
  (Subst.renaming pty.tvs) @. pty.ty

(** Type inference and type checking. This amounts to Algorithm W.

    [infer env t] returns a pair [(σ, ty)] such that term [t] has type [ty]
    under typing environment [σ(env)], where [σ] is a substitution.

    [check env t ty] returns a substitution [σ] such that term [t] has type
    [σ(ty)] under typing environment [σ(env)].

    These functions fail with a detailed error if the typing is not possible.
 **)

(* The language to type. *)
module L = RawLambda

let get_binding place env x =
  begin match Env.find x env with
  | pty                 ->  pty
  | exception Not_found ->  Error.error place "Unbound variable: %s" x
  end

let check_duplicates place xs =
  begin match Util.find_duplicate String.compare xs with
  | None   -> ()
  | Some x -> Error.error place "Variable bound several times in tuple-pattern: %s" x
  end

let rec infer (env : typenv) (t : L.term) : Subst.t * typ =
  begin match t.L.value with
  | L.Var x ->
      (Subst.id, instantiate (get_binding t.L.place env x))
  | L.Lam (x, t) ->
      let tv = fresh_typvar () in
      let (s, ty) = infer (Env.add x (polytyp_of_typ tv) env) t in
      (s, typ_arrow (s @. tv) ty)
  | L.App (t1, t2) ->
      (* Standard method: typing the function before the argument. *)
      (*let tv_param  = fresh_typvar ()
      and tv_return = fresh_typvar () in
      let s1 = check env t1 (typ_arrow tv_param tv_return) in
      let s2 = check (s1 @: env) t2 (s1 @. tv_param) in
      (s2 @ s1, s2 @. s1 @. tv_return)*)
      (* Or economical method: typing the argument before the function. *)
      let tv = fresh_typvar () in
      let (s2, ty2) = infer env t2 in
      let s1 = check (s2 @: env) t1 (typ_arrow ty2 tv) in
      (s1 @ s2, s1 @. tv)
  | L.Lit _ ->
      (Subst.id, typ_int)
  | L.BinOp (t1, _, t2) ->
      let s1 = check env t1 typ_int in
      let s2 = check (s1 @: env) t2 typ_int in
      (s2 @ s1, typ_int)
  | L.Print t1 ->
      let s1 = check env t1 typ_int in
      (s1, typ_int)
  (* This rule is only a shortcut, it would also be treated correctly by the
     next one: *)
  | L.Let (_, x, L.{ value = Var y ; place }, t) ->
      infer (Env.add x (get_binding place env y) env) t
  | L.Let (recursive, x, t1, t2) ->
      let tv = fresh_typvar () in
      let env' =
        begin match recursive with
        | L.NonRecursive ->  env
        | L.Recursive    ->  Env.add x (polytyp_of_typ tv) env
            (* Note: this does not implement polymorphic recursion. Anyway, type
               inference with polymorphic recursion is undecidable; an explicit
               polytype annotation would be needed. *)
        end in
      let s1 = check env' t1 tv in
      let ty1 = s1 @. tv in
      let env = s1 @: env in
      let pty1 =
        (* This is the value restriction: *)
        begin match t1.L.value with
        | L.Var _ | L.Lam _ -> generalize env ty1
        | _                 -> polytyp_of_typ ty1
        end in
      let (s2, ty2) = infer (Env.add x pty1 env) t2 in
      (s2 @ s1, ty2)
  | L.IfZero (t1, t2, t3) ->
      let s1 = check env t1 typ_int in
      let (s2, ty2) = infer (s1 @: env) t2 in
      let s21 = s2 @ s1 in
      let s3 = check (s21 @: env) t3 ty2 in
      (s3 @ s21, ty2)
  (* Extending the language with tuples: *)
  | L.Tup ts ->
      let (s, tys) =
        List.fold_left begin fun (s, tys) t ->
          let (s', ty') = infer (s @: env) t in
          (s' @ s, ty' :: List.map (Subst.apply_in_typ s') tys)
        end
          (Subst.id, [])
          ts
      in
      (s, typ_tuple (List.rev tys))
  | L.LetTup (xs, t1, t2) ->
      check_duplicates t.L.place xs ;
      let tvs = List.map (fun _  -> fresh_typvar ()) xs in
      let ty1 = typ_tuple tvs in
      let s1 = check env t1 ty1 in
      let tys = List.map (Subst.apply_in_typ s1) tvs in
      let env = s1 @: env in
      let ptys =
        (* This is the value restriction: *)
        begin match t1.L.value with
        | L.Var _ ->  List.map (generalize env) tys
        | _       ->  List.map polytyp_of_typ tys
        end in
      let zip = List.combine xs ptys in
      let env' = List.fold_left (fun e (x, pty) -> Env.add x pty e) env zip in
      let (s2, ty2) = infer env' t2 in
      (s2 @ s1, ty2)
  end

and check (env : typenv) (t : L.term) (ty : typ) : Subst.t =
  let (s, ty') = infer env t in
  begin match mgu (s @. ty) ty' with
  | u ->
      u @ s
  | exception Not_unifiable (u, ty1, ty2) ->
      let us = u @ s in
      let env = us @: env
      and ty  = us @. ty
      and ty' = u  @. ty'
      and ty1 = u  @. ty1
      and ty2 = u  @. ty2 in
      let ftv = TVSet.union_many (fun set -> set)
        [ ftv_typenv env ; ftv_typ ty ; ftv_typ ty' ; ftv_typ ty1 ; ftv_typ ty2 ] in
      let names = gen_names_for_typvars ftv in
      Error.error t.L.place
        "Type error: in typing environment:\n    %s\nthis type was expected:\n    %s\nbut that type has been inferred:\n    %s\nthis type:\n    %s\ndoes not unify with that type:\n    %s"
          (show_typenv ~names env)
          (show_typ    ~names ty)
          (show_typ    ~names ty')
          (show_typ    ~names ty1)
          (show_typ    ~names ty2)
  end

let typecheck_term (t : L.term) : L.term =
  let (_, _) = infer Env.empty t in
  t
