(* This intermediate language describes the result of defunctionalization.
   It retains the key features of the previous calculus, [Tail], in that
   the ordering of computations is explicit and every function call is a
   tail call. Furthermore, lambda-abstractions disappear. A memory block
   [Con] now contains an integer tag followed with a number of fields,
   which hold values. A [switch] construct appears, which allows testing
   the tag of a memory block. A number of (closed, mutually recursive)
   functions can be defined at the top level. *)

type tag =
  int

and variable =
  Atom.atom

and binop = Tail.binop =
  | OpAdd
  | OpSub
  | OpMul
  | OpDiv

and value = Tail.value =
  | VVar of variable
  | VLit of int
  | VBinOp of value * binop * value

(* A block contains an integer tag, followed with a number of fields. *)

and block =
  | Con of tag * value list

(* The construct [Swi (v, branches)] reads the integer tag stored in the
   memory block at address [v] and performs a case analysis on this tag,
   transferring control to the appropriate branch. (The value [v] should be a
   pointer to a memory block.) *)

and term =
  | Exit
  | TailCall of variable * value list
  | Print of value * term
  | LetVal of variable * value * term
  | LetBlo of variable * block * term
  | Swi of value * branch list
  | IfZero of value * term * term

(* A branch [tag xs -> t] is labeled with an integer tag [tag], and is
   executed if the memory block carries this tag. The variables [xs] are
   then bounds to the fields of the memory block. (The length of the list
   [xs] should be the number of fields of the memory block.) *)

and branch =
  | Branch of tag * variable list * term

(* A toplevel function declaration mentions the function's name, formal
   parameters, and body. *)

and function_declaration =
  | Fun of variable * variable list * term

(* A complete program consits of a set of toplevel function declarations
   and a term (the "main program"). The functions are considered mutually
   recursive: every function may refer to every function. *)

and program =
  | Prog of function_declaration list * term

[@@deriving show { with_path = false }]

(* -------------------------------------------------------------------------- *)

(* Constructor functions. *)

let vvar =
  Tail.vvar

let vvars =
  Tail.vvars

(* [let x_1 = v_1 in ... let x_n = v_n in t] *)

let rec sequential_let (xs : variable list) (vs : value list) (t : term) =
  match xs, vs with
  | [], [] ->
      t
  | x :: xs, v :: vs ->
      LetVal (x, v, sequential_let xs vs t)
  | _ ->
      assert false

(* [let x_1 = v_1 and ... x_n = v_n in t] *)

let parallel_let (xs : variable list) (vs : value list) (t : term) =
  assert (List.length xs = List.length vs);
  assert (Atom.Set.disjoint (Atom.Set.of_list xs) (Tail.fv_values vs));
  sequential_let xs vs t

(* -------------------------------------------------------------------------- *)

(* Counting the number of occurences of a variable [x]. *)

let rec count_in_value (x : variable) : value -> int = function
  | VVar y when Atom.equal x y ->
      1
  | VVar _
  | VLit _ ->
      0
  | VBinOp (v1, _, v2) ->
      count_in_value x v1 + count_in_value x v2

and count_in_values x vs =
  List.fold_left (+) 0 @@ List.map (count_in_value x) vs

and count_in_block (x : variable) : block -> int = function
  | Con (_, vs) ->
      count_in_values x vs

and count_in_term (x : variable) : term -> int = function
  | Exit ->
      0
  | TailCall (y, vs) ->
      let d = if Atom.equal x y then 1 else 0 in
      d + count_in_values x vs
  | Print (v, t) ->
      count_in_value x v + count_in_term x t
  | LetVal (y, v, t) ->
      assert (not @@ Atom.equal x y) ;
      count_in_value x v + count_in_term x t
  | LetBlo (y, b, t) ->
      assert (not @@ Atom.equal x y) ;
      count_in_block x b + count_in_term x t
  | Swi (v, branches) ->
      count_in_value x v + count_in_branches x branches
  | IfZero (v1, t2, t3) ->
      count_in_value x v1 + count_in_term x t2 + count_in_term x t3

and count_in_branch (x : variable) : branch -> int = function
  | Branch (_, ys, t) ->
      assert (not @@ List.exists (Atom.equal x) ys) ;
      count_in_term x t

and count_in_branches x bs =
  List.fold_left (+) 0 @@ List.map (count_in_branch x) bs

(* -------------------------------------------------------------------------- *)

(* Substitution of a value [s] for a variable [x]. This is completely
   straightforward (no α-renaming is needed) since all identifiers are
   guaranteed to be unique. *)

let rec subst_in_value (x : variable) (s : value) : value -> value = function
  | VVar y when Atom.equal x y ->
      s
  | VVar _ as v ->
      v
  | VLit _ as v ->
      v
  | VBinOp (v1, op, v2) ->
      VBinOp (subst_in_value x s v1, op, subst_in_value x s v2)

and subst_in_block (x : variable) (s : value) : block -> block = function
  | Con (tag, vs) ->
      Con (tag, List.map (subst_in_value x s) vs)

and subst_in_term (x : variable) (s : value) : term -> term = function
  | Exit ->
      Exit
  | TailCall (y, vs) when Atom.equal x y ->
      begin match s with
      | VVar z ->
          TailCall (z, List.map (subst_in_value x s) vs)
      | _ ->
          assert false (* forbidden by the type system *)
      end
  | TailCall (y, vs) ->
      TailCall (y, List.map (subst_in_value x s) vs)
  | Print (v, t) ->
      Print (subst_in_value x s v, subst_in_term x s t)
  | LetVal (y, v, t) ->
      assert (not @@ Atom.equal x y) ;
      LetVal (y, subst_in_value x s v, subst_in_term x s t)
  | LetBlo (y, b, t) ->
      assert (not @@ Atom.equal x y) ;
      LetBlo (y, subst_in_block x s b, subst_in_term x s t)
  | Swi (v, branches) ->
      Swi (subst_in_value x s v, List.map (subst_in_branch x s) branches)
  | IfZero (v1, t2, t3) ->
      IfZero (subst_in_value x s v1, subst_in_term x s t2, subst_in_term x s t3)

and subst_in_branch (x : variable) (s : value) : branch -> branch = function
  | Branch (tag, ys, t) ->
      assert (not @@ List.exists (Atom.equal x) ys) ;
      Branch (tag, ys, subst_in_term x s t)

(* -------------------------------------------------------------------------- *)

(* Simplification of terms. This eliminates renamings (variable-to-variable
   bindings). *)

let rec simplify_term : term -> term = function
  | Exit as t ->
      t
  | TailCall _ as t ->
      t
  | Print (v, t) ->
      Print (v, simplify_term t)
  | LetVal (x, v, t) ->
      let t' = simplify_term t in
      let do_subst =
        begin match v with
        | VVar _
        | VLit _ -> true
        | _      -> count_in_term x t' <= 1
        end in
      if do_subst then
        subst_in_term x v t'
      else
        LetVal (x, v, t')
  | LetBlo (x, b, t) ->
      let t' = simplify_term t in
      if count_in_term x t' = 0 then
        t'
      else
        LetBlo (x, b, t')
  | Swi (v, branches) ->
      Swi (v, List.map simplify_branch branches)
  | IfZero (v1, t2, t3) ->
      IfZero (v1, simplify_term t2, simplify_term t3)

and simplify_branch : branch -> branch = function
  | Branch (tag, xs, t) ->
      Branch (tag, xs, simplify_term t)

let simplify_declaration : function_declaration -> function_declaration = function
  | Fun (f, xs, t) ->
      Fun (f, xs, simplify_term t)

let simplify_program : program -> program = function
  | Prog (decls, t) ->
      Prog (List.map simplify_declaration decls, simplify_term t)
